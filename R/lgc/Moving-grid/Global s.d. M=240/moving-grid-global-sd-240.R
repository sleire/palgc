install.packages("tseries")
install.packages("lg")
install.packages("progress")
install.packages("PortfolioAnalytics")
library(PortfolioAnalytics)
library(tseries)
library(quadprog)
library(robustbase)
library(PerformanceAnalytics)
library(fPortfolio)
library(lg)
library(ggplot2)
library(progress)

install.packages("nycflights13")
install.packages("tidyverse")
library(nycflights13)
library(tidyverse)

#240 MONTHS
rm(list=ls())
QP_solver <- function(c, Q, cstr = list(), trace = FALSE) {
  
  Aeq <- Reduce(rbind, cstr[names(cstr) %in% "Aeq"])
  aeq <- Reduce(c, cstr[names(cstr) %in% "aeq"])
  A <- Reduce(rbind, cstr[names(cstr) %in% "A"])
  a <- Reduce(c, cstr[names(cstr) %in% "a"])
  
  sol <- try(solve.QP(Dmat = Q,
                      dvec = -2 * c,
                      Amat = t(rbind(Aeq, A)),
                      bvec = c(aeq, a),
                      meq = nrow(Aeq)),
             silent = TRUE)
  if (trace) cat(sol)
  if (inherits(sol, "try-error"))
    list(solution = rep(NA, length(c)), status = 1)
  else
    list(solution = sol$solution, status = 0)
}

data <- read.csv("C:/Users/Sverre Haugen/Desktop/Statistikk; Finansteori og Forsikringsmatematikk/Masteroppgave/Data/Data.csv",header=TRUE,sep=";")
data <- data[,-1]
attach(data)
head(data)
names(data)

data <- ts(data, start=c(1980,1), end=c(2018,8), frequency = 12)
data <- diff(log(data))
x <- data

# Training set. At time t, a rolling sampling window of size M=120 months is selected. 
x = window(data, end=c(2000,1))

# Test set. Use remaining data to test accuracy
y = window(data, start=c(2000,2))

#LGC moving-grid estimates using global s.d.
w1 <- array(0, dim=c(length(y[,1]),length(x[1,])))
w2 <- array(0, dim=c(length(y[,1]),length(x[1,])))
w3 <- array(0, dim=c(length(y[,1]),length(x[1,])))
w4 <- array(0, dim=c(length(y[,1]),length(x[1,])))

mu1 <- array(0, dim=c(length(y[,1]),1))
mu2 <- array(0, dim=c(length(y[,1]),1))
mu3 <- array(0, dim=c(length(y[,1]),1))
mu4 <- array(0, dim=c(length(y[,1]),1))

W01 <- array(0, dim=c(length(y[,1])+1,1))
W01[1,] <- 1
W02 <- array(0, dim=c(length(y[,1])+1,1))
W02[1,] <- 1
W03 <- array(0, dim=c(length(y[,1])+1,1))
W03[1,] <- 1
W04 <- array(0, dim=c(length(y[,1])+1,1))
W04[1,] <- 1

Moving_grid <- array(0, dim=c(length(y[,1]), 15))

#LGC moving-grid estimates using local-s.d.
w1_loc <- array(0, dim=c(length(y[,1]),length(x[1,])))
w2_loc <- array(0, dim=c(length(y[,1]),length(x[1,])))
w3_loc <- array(0, dim=c(length(y[,1]),length(x[1,])))
w4_loc <- array(0, dim=c(length(y[,1]),length(x[1,])))

mu1_loc <- array(0, dim=c(length(y[,1]),1))
mu2_loc <- array(0, dim=c(length(y[,1]),1))
mu3_loc <- array(0, dim=c(length(y[,1]),1))
mu4_loc <- array(0, dim=c(length(y[,1]),1))

W01_loc <- array(0, dim=c(length(y[,1])+1,1))
W01_loc[1,] <- 1
W02_loc <- array(0, dim=c(length(y[,1])+1,1))
W02_loc[1,] <- 1
W03_loc <- array(0, dim=c(length(y[,1])+1,1))
W03_loc[1,] <- 1
W04_loc <- array(0, dim=c(length(y[,1])+1,1))
W04_loc[1,] <- 1

#Target Reward
targetReturn <- function(x, target) {
  list(Aeq=rbind(colMeans(x)), aeq=target)
}

#Full Investment
fullInvest <- function(x) {
  list(Aeq=matrix(1, nrow=1, ncol=ncol(x)), aeq=1)
}

#Long Only
longOnly <- function(x) {
  list(A=diag(1, ncol(x)), a=rep(0, ncol(x)))
}

lgc <- array(0, dim=c(length(x[1,]),length(x[1,])))
lgcCov <- array(0, dim=c(length(x[1,]),length(x[1,])))
lgcCov_loc <- array(0, dim=c(length(x[1,]),length(x[1,])))
diag(lgc) <- 1

pftPerf <- function(y, w, W0=1) {
  W0*cumprod(c(1, 1+y %*% w))
}

pb <- progress_bar$new(total = dim(y)[1])
a=1
for(i in 1:dim(y)[1]) {
  #Local Gaussian correlation
  #NR:1 vs NR:2
  d <- abs(x[240,1]-x[240,2])
  if (x[240,1] < x[240,2]) {
    grid <- cbind(seq(x[240,1], x[240,2], d), seq(x[240,1], x[240,2], d))
  } else {
    grid <- cbind(seq(x[240,2], x[240,1], d), seq(x[240,2], x[240,1], d))
  }
  
  x1 <- x[,1:2]
  #Create an lg-object, that can be used to estimate local Gaussian correlations
  lg_object1 <- lg_main(x1, bw_method="cv", est_method = "5par",
                        transform_to_marginal_normality = FALSE)
  #Estimate a multivariate density function using locally Gaussian approximations
  density_estimate1 <- dlg(lg_object1, grid = grid)
  lgc[2,1] <- density_estimate1$loc_cor[1,]
  lgc[1,2] <- lgc[2,1]
  
  lgcCov[2,1] <- lgc[2,1]*(sd(x[,1])*sd(x[,2]))
  lgcCov[1,2] <- lgcCov[2,1]
  
  lgcCov_loc[2,1] <- lgc[2,1]*(density_estimate1$loc_sd[1,1]*density_estimate1$loc_sd[1,2])
  lgcCov_loc[1,2] <- lgcCov_loc[2,1]
  
  Moving_grid[i,1] <- density_estimate1$loc_cor[1,]
  
  #NR:1 vs NR:3
  d <- abs(x[240,1]-x[240,3])
  if (x[240,1] < x[240,3]) {
    grid <- cbind(seq(x[240,1], x[240,3], d), seq(x[240,1], x[240,3], d))
  } else {
    grid <- cbind(seq(x[240,3], x[240,1], d), seq(x[240,3], x[240,1], d))
  }
  
  x2 <- x[,1:2]
  x2[,2] <- x[,3]
  lg_object2 <- lg_main(x2, bw_method="cv", est_method = "5par",
                        transform_to_marginal_normality = FALSE)
  density_estimate2 <- dlg(lg_object2, grid = grid)
  lgc[3,1] <- density_estimate2$loc_cor[1,]
  lgc[1,3] <- lgc[3,1]
  
  lgcCov[3,1] <- lgc[3,1]*(sd(x[,1])*sd(x[,3]))
  lgcCov[1,3] <- lgcCov[3,1]
  
  lgcCov_loc[3,1] <- lgc[3,1]*(density_estimate2$loc_sd[1,1]*density_estimate2$loc_sd[1,2])
  lgcCov_loc[1,3] <- lgcCov_loc[3,1]
  
  Moving_grid[i,2] <- density_estimate2$loc_cor[1,]
  
  #NR:2 vs NR:3
  d <- abs(x[240,2]-x[240,3])
  if (x[240,2] < x[240,3]) {
    grid <- cbind(seq(x[240,2], x[240,3], d), seq(x[240,2], x[240,3], d))
  } else {
    grid <- cbind(seq(x[240,3], x[240,2], d), seq(x[240,3], x[240,2], d))
  }
  x3 <- x[,2:3]
  lg_object3 <- lg_main(x3, bw_method="cv", est_method = "5par",
                        transform_to_marginal_normality = FALSE)
  density_estimate3 <- dlg(lg_object3, grid = grid)
  lgc[3,2] <- density_estimate3$loc_cor[1,]
  lgc[2,3] <- lgc[3,2]
  
  lgcCov[3,2] <- lgc[3,2]*(sd(x[,2])*sd(x[,3]))
  lgcCov[2,3] <- lgcCov[3,2]
  
  lgcCov_loc[3,2] <- lgc[3,2]*(density_estimate3$loc_sd[1,1]*density_estimate3$loc_sd[1,2])
  lgcCov_loc[2,3] <- lgcCov_loc[3,2]
  
  Moving_grid[i,3] <- density_estimate3$loc_cor[1,]
  
  #NR:1 vs NR:4
  d <- abs(x[240,1]-x[240,4])
  if (x[240,1] < x[240,4]) {
    grid <- cbind(seq(x[240,1], x[240,4], d), seq(x[240,1], x[240,4], d))
  } else {
    grid <- cbind(seq(x[240,4], x[240,1], d), seq(x[240,4], x[240,1], d)) 
  }
  x4 <- x[,1:2]
  x4[,2] <- x[,4]
  lg_object4 <- lg_main(x4, bw_method="cv", est_method = "5par",
                        transform_to_marginal_normality = FALSE)
  density_estimate4 <- dlg(lg_object4, grid = grid)
  lgc[4,1] <- density_estimate4$loc_cor[1,]
  lgc[1,4] <- lgc[4,1]
  
  lgcCov[4,1] <- lgc[4,1]*(sd(x[,1])*sd(x[,4]))
  lgcCov[1,4] <- lgcCov[4,1]
  
  lgcCov_loc[4,1] <- lgc[4,1]*(density_estimate4$loc_sd[1,1]*density_estimate4$loc_sd[1,2])
  lgcCov_loc[1,4] <- lgcCov_loc[4,1]
  
  Moving_grid[i,4] <- density_estimate4$loc_cor[1,]
  
  #NR:2 vs NR:4
  d <- abs(x[240,2]-x[240,4])
  if (x[240,2] < x[240,4]) {
    grid <- cbind(seq(x[240,2], x[240,4], d), seq(x[240,2], x[240,4], d))
  } else {
    grid <- cbind(seq(x[240,4], x[240,2], d), seq(x[240,4], x[240,2], d))
  }
  x5 <- x[,3:4]
  x5[,1] <- x[,2]
  lg_object5 <- lg_main(x5, bw_method="cv", est_method = "5par",
                        transform_to_marginal_normality = FALSE)
  density_estimate5 <- dlg(lg_object5, grid = grid)
  lgc[4,2] <- density_estimate5$loc_cor[1,]
  lgc[2,4] <- lgc[4,2]
  
  lgcCov[4,2] <- lgc[4,2]*(sd(x[,2])*sd(x[,4]))
  lgcCov[2,4] <- lgcCov[4,2]
  
  lgcCov_loc[4,2] <- lgc[4,2]*(density_estimate5$loc_sd[1,1]*density_estimate5$loc_sd[1,2])
  lgcCov_loc[2,4] <- lgcCov_loc[4,2]
  
  Moving_grid[i,5] <- density_estimate5$loc_cor[1,]
  
  #NR:3 vs NR:4
  d <- abs(x[240,3]-x[240,4])
  if (x[240,3] < x[240,4]) {
    grid <- cbind(seq(x[240,3], x[240,4], d), seq(x[240,3], x[240,4], d))
  } else {
    grid <- cbind(seq(x[240,4], x[240,3], d), seq(x[240,4], x[240,3], d))
  }
  
  x6 <- x[,3:4]
  lg_object6 <- lg_main(x6, bw_method="cv", est_method = "5par",
                        transform_to_marginal_normality = FALSE)
  density_estimate6 <- dlg(lg_object6, grid = grid)
  lgc[4,3] <- density_estimate6$loc_cor[1,]
  lgc[3,4] <- lgc[4,3]
  
  lgcCov[4,3] <- lgc[4,3]*(sd(x[,3])*sd(x[,4]))
  lgcCov[3,4] <- lgcCov[4,3]
  
  lgcCov_loc[4,3] <- lgc[4,3]*(density_estimate6$loc_sd[1,1]*density_estimate6$loc_sd[1,2])
  lgcCov_loc[3,4] <- lgcCov_loc[4,3]
  
  Moving_grid[i,6] <- density_estimate6$loc_cor[1,]
  
  #NR:1 vs NR:5
  d <- abs(x[240,1]-x[240,5])
  if (x[240,1] < x[240,5]) {
    grid <- cbind(seq(x[240,1], x[240,5], d), seq(x[240,1], x[240,5], d))
  } else {
    grid <- cbind(seq(x[240,5], x[240,1], d), seq(x[240,5], x[240,1], d))
  }
  
  x7 <- x[,4:5]
  x7[,1] <- x[,1]
  lg_object7 <- lg_main(x7, bw_method="cv", est_method = "5par",
                        transform_to_marginal_normality = FALSE)
  density_estimate7 <- dlg(lg_object7, grid = grid)
  lgc[5,1] <- density_estimate7$loc_cor[1,]
  lgc[1,5] <- lgc[5,1]
  
  lgcCov[5,1] <- lgc[5,1]*(sd(x[,1])*sd(x[,5]))
  lgcCov[1,5] <- lgcCov[5,1]
  
  lgcCov_loc[5,1] <- lgc[5,1]*(density_estimate7$loc_sd[1,1]*density_estimate7$loc_sd[1,2])
  lgcCov_loc[1,5] <- lgcCov_loc[5,1]
  
  Moving_grid[i,7] <- density_estimate7$loc_cor[1,]
  
  #NR:2 vs NR:5
  d <- abs(x[240,2]-x[240,5])
  if (x[240,2] < x[240,5]) {
    grid <- cbind(seq(x[240,2], x[240,5], d), seq(x[240,2], x[240,5], d))
  } else {
    grid <- cbind(seq(x[240,5], x[240,2], d), seq(x[240,5], x[240,2], d))
  }
  
  x8 <- x[,4:5]
  x8[,1] <- x[,2]
  lg_object8 <- lg_main(x8, bw_method="cv", est_method = "5par",
                        transform_to_marginal_normality = FALSE)
  density_estimate8 <- dlg(lg_object8, grid = grid)
  lgc[5,2] <- density_estimate8$loc_cor[1,]
  lgc[2,5] <- lgc[5,2]
  
  lgcCov[5,2] <- lgc[5,1]*(sd(x[,2])*sd(x[,5]))
  lgcCov[2,5] <- lgcCov[5,2]
  
  lgcCov_loc[5,2] <- lgc[5,2]*(density_estimate8$loc_sd[1,1]*density_estimate8$loc_sd[1,2])
  lgcCov_loc[2,5] <- lgcCov_loc[5,2]
  
  Moving_grid[i,8] <- density_estimate8$loc_cor[1,]
  
  #NR:3 vs NR:5
  d <- abs(x[240,3]-x[240,5])
  if (x[240,3] < x[240,5]) {
    grid <- cbind(seq(x[240,3], x[240,5], d), seq(x[240,3], x[240,5], d))
  } else {
    grid <- cbind(seq(x[240,5], x[240,3], d), seq(x[240,5], x[240,3], d))
  }
  
  x9 <- x[,4:5]
  x9[,1] <- x[,3]
  lg_object9 <- lg_main(x9, bw_method="cv", est_method = "5par",
                        transform_to_marginal_normality = FALSE)
  density_estimate9 <- dlg(lg_object9, grid = grid)
  lgc[5,3] <- density_estimate9$loc_cor[1,]
  lgc[3,5] <- lgc[5,3]
  
  lgcCov[5,3] <- lgc[5,3]*(sd(x[,3])*sd(x[,5]))
  lgcCov[3,5] <- lgcCov[5,3]
  
  lgcCov_loc[5,3] <- lgc[5,3]*(density_estimate9$loc_sd[1,1]*density_estimate9$loc_sd[1,2])
  lgcCov_loc[3,5] <- lgcCov_loc[5,3]
  
  Moving_grid[i,9] <- density_estimate9$loc_cor[1,]
  
  #NR:4 vs NR:5
  d <- abs(x[240,4]-x[240,5])
  if (x[240,4] < x[240,5]) {
    grid <- cbind(seq(x[240,4], x[240,5], d), seq(x[240,4], x[240,5], d))
  } else {
    grid <- cbind(seq(x[240,5], x[240,4], d), seq(x[240,5], x[240,4], d))
  }
  
  x10 <- x[,4:5]
  lg_object10 <- lg_main(x10, bw_method="cv", est_method = "5par",
                         transform_to_marginal_normality = FALSE)
  density_estimate10 <- dlg(lg_object10, grid = grid)
  lgc[5,4] <- density_estimate10$loc_cor[1,]
  lgc[4,5] <- lgc[5,4]
  
  lgcCov[5,4] <- lgc[5,4]*(sd(x[,4]*sd(x[,5])))
  lgcCov[4,5] <- lgcCov[5,4]
  
  lgcCov_loc[5,4] <- lgc[5,4]*(density_estimate10$loc_sd[1,1]*density_estimate10$loc_sd[1,2])
  lgcCov_loc[4,5] <- lgcCov_loc[5,4]
  
  Moving_grid[i,10] <- density_estimate10$loc_cor[1,]
  
  #NR:1 vs NR:6
  d <- abs(x[240,1]-x[240,6])
  if (x[240,1] < x[240,6]) {
    grid <- cbind(seq(x[240,1], x[240,6], d), seq(x[240,1], x[240,6], d))
  } else {
    grid <- cbind(seq(x[240,6], x[240,1], d), seq(x[240,6], x[240,1], d))
  }
  
  x11 <- x[,5:6]
  x11[,1] <- x[,1]
  lg_object11 <- lg_main(x11, bw_method="cv", est_method = "5par",
                         transform_to_marginal_normality = FALSE)
  density_estimate11 <- dlg(lg_object11, grid = grid)
  lgc[6,1] <- density_estimate11$loc_cor[1,]
  lgc[1,6] <- lgc[6,1]
  
  lgcCov[6,1] <- lgc[6,1]*(sd(x[,1])*sd(x[,6]))
  lgcCov[1,6] <- lgcCov[6,1]
  
  lgcCov_loc[6,1] <- lgc[6,1]*(density_estimate11$loc_sd[1,1]*density_estimate11$loc_sd[1,2])
  lgcCov_loc[1,6] <- lgcCov_loc[6,1]
  
  Moving_grid[i,11] <- density_estimate11$loc_cor[1,]
  
  #NR:2 vs NR:6
  d <- abs(x[240,2]-x[240,6])
  if (x[240,2] < x[240,6]) {
    grid <- cbind(seq(x[240,2], x[240,6], d), seq(x[240,2], x[240,6], d))
  } else {
    grid <- cbind(seq(x[240,6], x[240,2], d), seq(x[240,6], x[240,2], d))
  }
  
  x12 <- x[,5:6]
  x12[,1] <- x[,2]
  lg_object12 <- lg_main(x12, bw_method="cv", est_method = "5par",
                         transform_to_marginal_normality = FALSE)
  density_estimate12 <- dlg(lg_object12, grid = grid)
  lgc[6,2] <- density_estimate12$loc_cor[1,]
  lgc[2,6] <- lgc[6,2]
  
  lgcCov[6,2] <- lgc[6,2]*(sd(x[,2])*sd(x[,6]))
  lgcCov[2,6] <- lgcCov[6,2]
  
  lgcCov_loc[6,2] <- lgc[6,2]*(density_estimate12$loc_sd[1,1]*density_estimate12$loc_sd[1,2])
  lgcCov_loc[2,6] <- lgcCov_loc[6,2]
  
  Moving_grid[i,12] <- density_estimate12$loc_cor[1,]
  
  #NR:3 vs NR:6
  d <- abs(x[240,3]-x[240,6])
  if (x[240,3] < x[240,6]) {
    grid <- cbind(seq(x[240,3], x[240,6], d), seq(x[240,3], x[240,6], d))
  } else {
    grid <- cbind(seq(x[240,6], x[240,3], d), seq(x[240,6], x[240,3], d))
  }
  x13 <- x[,5:6]
  x13[,1] <- x[,3]
  lg_object13 <- lg_main(x13, bw_method="cv", est_method = "5par",
                         transform_to_marginal_normality = FALSE)
  density_estimate13 <- dlg(lg_object13, grid = grid)
  lgc[6,3] <- density_estimate12$loc_cor[1,]
  lgc[3,6] <- lgc[6,3]
  
  lgcCov[6,3] <- lgc[6,3]*(sd(x[,3])*sd(x[,6]))
  lgcCov[3,6] <- lgcCov[6,3]
  
  lgcCov_loc[6,3] <- lgc[6,3]*(density_estimate13$loc_sd[1,1]*density_estimate13$loc_sd[1,2])
  lgcCov_loc[3,6] <- lgcCov_loc[6,3]
  
  Moving_grid[i,13] <- density_estimate13$loc_cor[1,]
  
  #NR:4 vs NR:6
  d <- abs(x[240,4]-x[240,6])
  if (x[240,4] < x[240,6]) {
    grid <- cbind(seq(x[240,4], x[240,6], d), seq(x[240,4], x[240,6], d))
  } else {
    grid <- cbind(seq(x[240,6], x[240,4], d), seq(x[240,6], x[240,4], d))
  }
  
  x14 <- x[,5:6]
  x14[,1] <- x[,4]
  lg_object14 <- lg_main(x14, bw_method="cv", est_method = "5par",
                         transform_to_marginal_normality = FALSE)
  density_estimate14 <- dlg(lg_object14, grid = grid)
  lgc[6,4] <- density_estimate14$loc_cor[1,]
  lgc[4,6] <- lgc[6,4]
  
  lgcCov[6,4] <- lgc[6,4]*(sd(x[,4])*sd(x[,6]))
  lgcCov[4,6] <- lgcCov[6,4]
  
  lgcCov_loc[6,4] <- lgc[6,4]*(density_estimate14$loc_sd[1,1]*density_estimate14$loc_sd[1,2])
  lgcCov_loc[4,6] <- lgcCov_loc[6,4]
  
  Moving_grid[i,14] <- density_estimate14$loc_cor[1,]
  
  #NR:5 vs NR:6
  d <- abs(x[240,5]-x[240,6])
  if (x[240,5] < x[240,6]) {
    grid <- cbind(seq(x[240,5], x[240,6], d), seq(x[240,5], x[240,6], d))
  } else {
    grid <- cbind(seq(x[240,6], x[240,5], d), seq(x[240,6], x[240,5], d))
  }
  
  x15 <- x[,5:6]
  lg_object15 <- lg_main(x15, bw_method="cv", est_method = "5par",
                         transform_to_marginal_normality = FALSE)
  density_estimate15 <- dlg(lg_object15, grid = grid)
  lgc[6,5] <- density_estimate15$loc_cor[1,]
  lgc[5,6] <- lgc[6,5]
  
  lgcCov[6,5] <- lgc[6,5]*(sd(x[,5])*sd(x[,6]))
  lgcCov[5,6] <- lgcCov[6,5]
  
  lgcCov_loc[6,5] <- lgc[6,5]*(density_estimate15$loc_sd[1,1]*density_estimate15$loc_sd[1,2])
  lgcCov_loc[5,6] <- lgcCov_loc[6,5]
  
  Moving_grid[i,15] <- density_estimate15$loc_cor[1,]
  
  #DIAGONAL
  diag(lgcCov) <- diag(cov(x))
  diag(lgcCov_loc) <- diag(cov(x))
  
  MV_QP <- function(x, target, Sigma=lgcCov, ...,
                    cstr=c(fullInvest(x), 
                           targetReturn(x, target), 
                           longOnly(x), ...),
                    trace=FALSE) {
    
    #quadratic coefficients
    size <- ncol(x)
    c <- rep(0, size)
    Q <- Sigma
    
    #optimization
    sol <- QP_solver(c, Q, cstr, trace)
    
    #extract weights
    weights <- sol$solution
    names(weights) <- colnames(x)
    weights
  }
  
  #Mean-Variance
  w1[i,] <- MV_QP(x, mean(x), Sigma=lgcCov) #MVS
  w2[i,] <- MV_QP(x, mean(x), Sigma=lgcCov, cstr = c(fullInvest(x), targetReturn(x, mean(x)))) #MVS
  #Minimum Variance Portfolio
  w3[i,] <- MV_QP(x, Sigma=lgcCov, cstr = c(fullInvest(x), longOnly(x))) #MINC
  w4[i,] <- MV_QP(x, Sigma=lgcCov, cstr = c(fullInvest(x))) #MVS
  
  #Cumulative Performance
  mu1[i,] <- sum((W01[i,]*w1[i,])*y[i,])
  W01[i+1,] <- W01[i,]+mu1[i,]

  mu2[i,] <- sum((W02[i,]*w2[i,])*y[i,])
  W02[i+1,] <- W02[i,]+mu2[i,]

  mu3[i,] <- sum((W03[i,]*w3[i,])*y[i,])
  W03[i+1,] <- W03[i,]+mu3[i,]

  mu4[i,] <- sum((W04[i,]*w4[i,])*y[i,])
  W04[i+1,] <- W04[i,]+mu4[i,]
  
  MV_QP <- function(x, target, Sigma=lgcCov_loc, ...,
                    cstr=c(fullInvest(x), 
                           targetReturn(x, target), 
                           longOnly(x), ...),
                    trace=FALSE) {
    
    #quadratic coefficients
    size <- ncol(x)
    c <- rep(0, size)
    Q <- Sigma
    
    #optimization
    sol <- QP_solver(c, Q, cstr, trace)
    
    #extract weights
    weights <- sol$solution
    names(weights) <- colnames(x)
    weights
  }
  
  #Mean-Variance
  w1_loc[i,] <- MV_QP(x, mean(x), Sigma=lgcCov_loc) #MVS
  w2_loc[i,] <- MV_QP(x, mean(x), Sigma=lgcCov_loc, cstr = c(fullInvest(x), targetReturn(x, mean(x)))) #MVS
  #Minimum Variance Portfolio
  w3_loc[i,] <- MV_QP(x, Sigma=lgcCov_loc, cstr = c(fullInvest(x), longOnly(x))) #MINC
  w4_loc[i,] <- MV_QP(x, Sigma=lgcCov_loc, cstr = c(fullInvest(x))) #MVS
  
  #Cumulative Performance
  mu1_loc[i,] <- sum((W01_loc[i,]*w1_loc[i,])*y[i,])
  W01_loc[i+1,] <- W01_loc[i,]+mu1_loc[i,]

  mu2_loc[i,] <- sum((W02_loc[i,]*w2_loc[i,])*y[i,])
  W02_loc[i+1,] <- W02_loc[i,]+mu2_loc[i,]

  mu3_loc[i,] <- sum((W03_loc[i,]*w3_loc[i,])*y[i,])
  W03_loc[i+1,] <- W03_loc[i,]+mu3_loc[i,]

  mu4_loc[i,] <- sum((W04_loc[i,]*w4_loc[i,])*y[i,])
  W04_loc[i+1,] <- W04_loc[i,]+mu4_loc[i,]
  
  x <- rbind(x, y[a,])
  x <- x[-1,]
  
  a=a+1
  pb$tick()
  Sys.sleep(1 / dim(y)[1])
}

options(digits=5)
#LGC moving-grid estimates using global s.d.
sd1r <- sd(mu1)
sd2r <- sd(mu2)
sd3r <- sd(mu3)
sd4r <- sd(mu4)

mu11 <- sum(mu1)/length(w1[,1])
mu21 <- sum(mu2)/length(w1[,1])
mu31 <- sum(mu3)/length(w1[,1])
mu41 <- sum(mu4)/length(w1[,1])

#Sharpe Ratio
SR11 <- mu11/sd1r
SR21 <- mu21/sd2r
SR31 <- mu31/sd3r
SR41 <- mu41/sd4r

CEQ11 <- (mu11-(1/2)*sd1r^2)*100
CEQ21 <- (mu21-(1/2)*sd2r^2)*100
CEQ31 <- (mu31-(1/2)*sd3r^2)*100
CEQ41 <- (mu41-(1/2)*sd4r^2)*100

mu1 <- mu11*100
mu2 <- mu21*100
mu3 <- mu31*100
mu4 <- mu41*100

sd11<-0
sd21<-0
sd31<-0
sd41<-0

for(i in 1:length(w1[,1])) {
  sd11 <- sd11 + sqrt((1/6)*((w1[i,1]-sum(w1[i,]))^2
                             +(w1[i,2]-sum(w1[i,]))^2
                             +(w1[i,3]-sum(w1[i,]))^2
                             +(w1[i,4]-sum(w1[i,]))^2
                             +(w1[i,5]-sum(w1[i,]))^2
                             +(w1[i,6]-sum(w1[i,]))^2))
  
  sd21 <- sd21 + sqrt((1/6)*((w2[i,1]-sum(w2[i,]))^2
                             +(w2[i,2]-sum(w2[i,]))^2
                             +(w2[i,3]-sum(w2[i,]))^2
                             +(w2[i,4]-sum(w2[i,]))^2
                             +(w2[i,5]-sum(w2[i,]))^2
                             +(w2[i,6]-sum(w2[i,]))^2))
  
  sd31 <- sd31 + sqrt((1/6)*((w3[i,1]-sum(w3[i,]))^2
                             +(w3[i,2]-sum(w3[i,]))^2
                             +(w3[i,3]-sum(w3[i,]))^2
                             +(w3[i,4]-sum(w3[i,]))^2
                             +(w3[i,5]-sum(w3[i,]))^2
                             +(w3[i,6]-sum(w3[i,]))^2))
  
  sd41 <- sd41 + sqrt((1/6)*((w4[i,1]-sum(w4[i,]))^2
                             +(w4[i,2]-sum(w4[i,]))^2
                             +(w4[i,3]-sum(w4[i,]))^2
                             +(w4[i,4]-sum(w4[i,]))^2
                             +(w4[i,5]-sum(w4[i,]))^2
                             +(w4[i,6]-sum(w4[i,]))^2))
}

sd11 <- sd11/length(w1[,1])
sd21 <- sd21/length(w1[,1])
sd31 <- sd31/length(w1[,1])
sd41 <- sd41/length(w1[,1])

#LGC moving-grid estimates using local s.d.
sd1r <- sd(mu1_loc)
sd2r <- sd(mu2_loc)
sd3r <- sd(mu3_loc)
sd4r <- sd(mu4_loc)

mu11_loc <- sum(mu1_loc)/length(w1[,1])
mu21_loc <- sum(mu2_loc)/length(w1[,1])
mu31_loc <- sum(mu3_loc)/length(w1[,1])
mu41_loc <- sum(mu4_loc)/length(w1[,1])

#Sharpe Ratio
SR11 <- mu11_loc/sd1r
SR21 <- mu21_loc/sd2r
SR31 <- mu31_loc/sd3r
SR41 <- mu41_loc/sd4r

CEQ11 <- (mu11_loc-(1/2)*sd1r^2)*100
CEQ21 <- (mu21_loc-(1/2)*sd2r^2)*100
CEQ31 <- (mu31_loc-(1/2)*sd3r^2)*100
CEQ41 <- (mu41_loc-(1/2)*sd4r^2)*100

mu1_loc <- mu11_loc*100
mu2_loc <- mu21_loc*100
mu3_loc <- mu31_loc*100
mu4_loc <- mu41_loc*100

sd11<-0
sd21<-0
sd31<-0
sd41<-0

for(i in 1:length(w1[,1])) {
  sd11 <- sd11 + sqrt((1/6)*((w1_loc[i,1]-sum(w1_loc[i,]))^2
                             +(w1_loc[i,2]-sum(w1_loc[i,]))^2
                             +(w1_loc[i,3]-sum(w1_loc[i,]))^2
                             +(w1_loc[i,4]-sum(w1_loc[i,]))^2
                             +(w1_loc[i,5]-sum(w1_loc[i,]))^2
                             +(w1_loc[i,6]-sum(w1_loc[i,]))^2))
  
  sd21 <- sd21 + sqrt((1/6)*((w2_loc[i,1]-sum(w2_loc[i,]))^2
                             +(w2_loc[i,2]-sum(w2_loc[i,]))^2
                             +(w2_loc[i,3]-sum(w2_loc[i,]))^2
                             +(w2_loc[i,4]-sum(w2_loc[i,]))^2
                             +(w2_loc[i,5]-sum(w2_loc[i,]))^2
                             +(w2_loc[i,6]-sum(w2_loc[i,]))^2))
  
  sd31 <- sd31 + sqrt((1/6)*((w3_loc[i,1]-sum(w3_loc[i,]))^2
                             +(w3_loc[i,2]-sum(w3_loc[i,]))^2
                             +(w3_loc[i,3]-sum(w3_loc[i,]))^2
                             +(w3_loc[i,4]-sum(w3_loc[i,]))^2
                             +(w3_loc[i,5]-sum(w3_loc[i,]))^2
                             +(w3_loc[i,6]-sum(w3_loc[i,]))^2))
  
  sd41 <- sd41 + sqrt((1/6)*((w4_loc[i,1]-sum(w4_loc[i,]))^2
                             +(w4_loc[i,2]-sum(w4_loc[i,]))^2
                             +(w4_loc[i,3]-sum(w4_loc[i,]))^2
                             +(w4_loc[i,4]-sum(w4_loc[i,]))^2
                             +(w4_loc[i,5]-sum(w4_loc[i,]))^2
                             +(w4_loc[i,6]-sum(w4_loc[i,]))^2))
}

sd11 <- sd11/length(w1[,1])
sd21 <- sd21/length(w1[,1])
sd31 <- sd31/length(w1[,1])
sd41 <- sd41/length(w1[,1])



