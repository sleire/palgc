FROM rocker/rstudio:3.6.2
 
RUN apt-get update -qq && apt-get -y --no-install-recommends install \
libxml2-dev \
libxt-dev \
libjpeg-dev \
libglu1-mesa-dev \
libcairo2-dev \
libsqlite3-dev \
libpq-dev \
libssh2-1-dev \
unixodbc-dev \
&& install2.r --error \
--deps TRUE \
data.table \
lg \
fGarch \
xtable \
progress \
quadprog \
stargazer \
PerformanceAnalytics