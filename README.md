# palgc

Portfolio Allocation under Asymmetric Dependence using Local Gaussian Correlation

**Reproduce environment and results in Docker**

Clone repository to local folder, for example /home/myuser/palgc and build container image from project root

`docker build --tag palgc .`

Run the container and mount local palgc folder to container palgc folder

`docker run -d  -e PASSWORD=palgc -p 5000:8787 --name palgc -v /home/myuser/palgc:/home/rstudio/palgc palgc`

Login Rstudio with username `rstudio` and password `palgc` at

`http://0.0.0.0:5000`

Reproduce results by running `run_pipeline.R`

Shut down container with

`docker stop palgc`